<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="GitLab Pages">
  <title>test</title>
  <link rel="stylesheet" href="style.css" />
</head>
<body>
  <form id="form" method="POST" action="http://u20969.kubsu-dev.ru/bac6/public/index.php">
    <?php /*
    if (!empty($_COOKIE['save'])) {
        print('<p>результаты отправдены</p></form></body></html>');
        //setcookie('save', '', 100000);

        exit();
    }
    */ ?>
    <label>
      <?php
      if(!empty($errors['name'])) {
          print('<p style="color: red;">'.$errors['name'].'</p>');
      }
      ?>
      имя: <input name="name" placeholder="введите имя" <?php
      if(!empty($values['name'])) print(' value="'.$values['name'].'" ');
      ?>/>
    </label><br />
    
    <label>
      <?php
      if(!empty($errors['email'])) {
          print('<p style="color: red;">'.$errors['email'].'</p>');
      }
      ?>
      e-mail: <input type="email" name="email" placeholder="e-mail"<?php
      if(!empty($values['email'])) print(' value="'.$values['email'].'" ');
      ?>/>
    </label><br />
    
    <label>
      дата рождения: <input type="date" name="date" 
        min="1900-01-01" max="2050-01-01" <?php
      if(!empty($values['date'])) print(' value="'.$values['date'].'" ');
      else print(' value="2000-01-01" ');
      ?>/>
    </label><br />
		
    <div>пол: 
      <label>
        <input type="radio" name="gender" value="1" <?php
      if(!empty($values['gender'])&&$values['gender']=='1'||empty($values['gender'])) print(' checked="checked" ');
      ?> />м
      </label>
      <label><input type="radio" name="gender" value="2" <?php
      if(!empty($values['gender'])&&$values['gender']=='2') print(' checked="checked" ');
      ?>/>ж
      </label>
    </div><br /><br />
		
    <div>количество конечностей: <br />
      <label><input type="radio" name="numOfLimbs" value="0"<?php
      if(!empty($values['numOfLimbs'])&&$values['numOfLimbs']=='0') print(' checked="checked" ');
      ?> />0</label> 
      <label><input type="radio" name="numOfLimbs" value="1" <?php
      if(!empty($values['numOfLimbs'])&&$values['numOfLimbs']=='1') print(' checked="checked" ');
      ?>/>1</label> 
      <label><input type="radio" name="numOfLimbs" value="2" <?php
      if(!empty($values['numOfLimbs'])&&$values['numOfLimbs']=='2') print(' checked="checked" ');
      ?>/>2</label> 
      <label><input type="radio" name="numOfLimbs" value="3" <?php
      if(!empty($values['numOfLimbs'])&&$values['numOfLimbs']=='3') print(' checked="checked" ');
      ?>/>3</label> 
      <label>
        <input type="radio" name="numOfLimbs" value="4" <?php
      if(!empty($values['numOfLimbs'])&&$values['numOfLimbs']=='4'||empty($values['numOfLimbs'])) print(' checked="hecked" ');
      ?> />4
      </label> 
    </div><br /><br />
		
    <div>сверхспособности:<br />
      <select name="superpowers[]" multiple="multiple">
        <option value="бессмертие"<?php
          if(!empty($values['superpowers_1'])) print(' selected ');
        ?>>бессмертие</option>
        <option value="хождение сквозь стены"<?php
          if(!empty($values['superpowers_2'])) print(' selected ');
        ?>>хождение сквозь стены</option>
        <option value="левитация"<?php
          if(!empty($values['superpowers_3'])) print(' selected ');
        ?>>левитация</option>
      </select>
    </div><br /><br />
		
    <label>
      <?php
      if(!empty($errors['biography'])) {
          print('<p style="color: red;">'.$errors['biography'].'</p>');
      }
      ?>
      биография: <br />
      <textarea name="biography"><?php
      if(!empty($values['biography'])) print($values['biography']);
      ?></textarea>
    </label><br /><br />
		
    <div>
    <?php
    if(!empty($errors['checkbox'])) {
        print('<p style="color: red;">'.$errors['checkbox'].'</p>');
    }
    ?>
      с контрактом ознакомлен: <input type="checkbox" name="checkbox" value="_" <?php
        if(!empty($values['checkbox'])) print(' checked="checked" ');
      ?>/>
  	</div><br /><br />
    <input type="submit" value="отправить" />
    <br />
    <a href="http://u20969.kubsu-dev.ru/bac6/public/admin.php">вход для администраторов</a>
    <br />
  </form>
</body>
</html>
